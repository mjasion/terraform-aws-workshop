# Terraform AWS Workshop

## Przygotowanie

**Co musicie przygotować na warsztat:**

1. Laptop z IDEĄ + plugin do terraforma. Otwórzcie to repo i otwórzcie plik aws.tf - IDEA sama wykryje
2. Terraform (Instrukcja na linuksa)
```
- Pobierzcie stąd https://www.terraform.io/downloads.html
- Rozpakujcie
- chmod +x terraform
- mv terraform /usr/local/bin
```
3. Załóżcie sobie konto na AWS (o ile nie macie jakiegoś swojego z credytami) - jest free tier itd


Pobieramy klucze API oraz Account ID

1. `cp my.tfvars.template my.tfvars` i wstawiamy puste pola
2. Account ID można pobrać stąd https://console.aws.amazon.com/billing/home?#/account
3. API Key generujemy tu: https://console.aws.amazon.com/iam/home#/security_credential w panelu `Access keys (access key ID and secret access key)`

## Pierwsze odpalenie

```bash
terraform init
terraform plan -var-file=my.tfvar
terraform apply -var-file=my.tfvar
#terraform destroy -var-file=my.tfvar
```

## Zanim zaczniemy to wyklikajcie AWS CloudFront. Przyda się za chwilę, a samo założenie trwa kilka minut niestety
1. Załóżcie kubełek [S3](https://s3.console.aws.amazon.com/s3/home?region=eu-west-1) o **LOSOWEJ** nazwie. I nie sciągajcie od siebie
![](screens/bardzolosowykubelek.png)


Przejdzcie do [AWS CloudFront](https://console.aws.amazon.com/cloudfront/home)
1. `Create Distribution`
2. Web -> `Get Started`
3.  Ustawiamy [Screen](screens/cloudfront.png):
    ```
        Origin Domain Name: bardzolosowanazwa.s3.amazonaws.com
        Origins path: PUSTE
        Origin-ID: S3-bardzolosowanazwa
        State: Enabled
    ``` 
4. `Create Distribution`

## Lekcja 1 - Tworzymy kubełek S3
1. Tworzymy plik `s3.tf` i wklejamy:
    ```
    resource "aws_s3_bucket" "terraform-workshop" {
        bucket = ""
        acl = "private"
        region = "eu-west-1"
    
        website {
          index_document = "index.html"
        }
    
    }
    
    resource "aws_s3_bucket_policy" "terraform-workshop" {
      bucket = "${aws_s3_bucket.terraform-workshop.bucket}"
      policy = <<POLICY
    {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Sid": "AllowReadObjects",
          "Effect": "Allow",
          "Principal": "*",
          "Action": "s3:GetObject",
          "Resource": "${aws_s3_bucket.terraform-workshop.arn}/*"
        }
      ]
    }
    POLICY
    }
    
    output "Twoja strona jest dostepna pod adresem" {
      value = "http://${aws_s3_bucket.terraform-workshop.bucket}.${aws_s3_bucket.terraform-workshop.website_domain}"
    }
    ```
2. Edytujemy linijkę `bucket = ""` gdzie możemy zdefiniować swoją nazwę kubełka. Musi być unikalna
3. Przez przeglądarkę przenosimy pliki z folderu static_site (Drag&Drop)
4. Wchodzimy na adres który wyświetlił się w konsoli np.
    ```
    Twoja strona z kubelka S3 jest dostepna pod adresem = http://bardzolosowanazwa.s3-website-eu-west-1.amazonaws.com
    ```

I widzimy naszą stronę :-)

## Lekcja 2 - Spinamy sobie z cloudfrontem 

1. Wracamy do [AWS CloudFront](https://console.aws.amazon.com/cloudfront/home)
Cloudfront prawdopodobnie nie skonczył się tworzyć. Ale ważne że status już Enabled :-)

![](screens/cloudfron_prawie_dziala.png)

2. Tworzymy plik cloudfront.tf i tam umieszczamy:
    ```resource "aws_cloudfront_distribution" "terraform-workshop" {
      origin {
        domain_name = "${aws_s3_bucket.terraform-workshop.bucket_domain_name}"
        origin_id = "S3_${aws_s3_bucket.terraform-workshop.bucket}"
    
        s3_origin_config {
          origin_access_identity = "${aws_cloudfront_origin_access_identity.terraform-workshop.cloudfront_access_identity_path}"
        }
      }
    
      enabled = true
    
      is_ipv6_enabled = true
      comment = "${aws_s3_bucket.terraform-workshop.bucket}"
      default_root_object = "index.html"
      price_class = "PriceClass_100" // # najtaniej
      http_version = "http2"
    
      restrictions {
        "geo_restriction" {
          restriction_type = "none"
        }
      }
    
      default_cache_behavior {
        allowed_methods = [
          "DELETE",
          "GET",
          "HEAD",
          "OPTIONS",
          "PATCH",
          "POST",
          "PUT"
        ]
        cached_methods = [
          "GET",
          "HEAD"
        ]
        target_origin_id = "S3_${aws_s3_bucket.terraform-workshop.bucket}"
    
        forwarded_values {
          query_string = false
    
          cookies {
            forward = "none"
          }
        }
    
        viewer_protocol_policy = "redirect-to-https"
        min_ttl = "0"
        default_ttl = "60"
        max_ttl = "300"
      }
      viewer_certificate {
        cloudfront_default_certificate = true
      }
    
    }
    
    resource "aws_cloudfront_origin_access_identity" "terraform-workshop" {
      comment = "${aws_s3_bucket.terraform-workshop.bucket}"
    }
    
    output "Twoja strona z CDN jest dostepna pod adresem" {
      value = "https://${aws_cloudfront_distribution.terraform-workshop.domain_name}"
    }
    ```
3. kopiujemy z przegladarki ID (np. `E2ZFC7SKA85ZG3`) i w konsoli odpalamy komendę
    ```
    terraform import -var-file=my.tfvars aws_cloudfront_distribution.terraform-workshop E2ZFC7SKA85ZG3
    ```
4. Robimy Apply
5. Tłumaczę co sie stało
6. wchodzimy na stronę która wyświeliła się w konsoli obok 
    ```
    Twoja strona z CDN jest dostepna pod adresem = 
    ```
7. Jeśli udało się, ze strona pod adresem jest dostepna to super. 
    
    Jak zwracane jest 307 i przekierowywani jesteśmy pod adres S3 to jest prawie ok. Choć poprawne działanie zajmie do 24 godzin ;/

## Lekcja 3 - Stawiamy wirtualki

1. Tworzymy plik `ec2.tf`
    ```
    resource "aws_key_pair" "terraform-workshop" {
      key_name   = "terraform-workshop"
      public_key = "ssh-rsa . terraform-workshop"
    }
    
    resource "aws_instance" "terraform-workshop" {
      ami = "ami-8fd760f6"
      instance_type = "t2.micro"
      key_name = "${aws_key_pair.terraform-workshop.id}"
      disable_api_termination = false
    
      vpc_security_group_ids = [
        "${aws_security_group.terraform-workshop.id}"
      ]
    
      root_block_device {
        volume_size = 8
        volume_type = "gp2"
        delete_on_termination = true
      }
    
    }
    resource "aws_security_group" "terraform-workshop" {
      name        = "Terraform Workshop"
      description = "Terraform Workshop"
    
      ingress {
        from_port   = -1
        to_port     = -1
        protocol    = "icmp"
        cidr_blocks = [
          "0.0.0.0/0",
        ]
      }
    
      ingress {
        from_port   = 0
        to_port     = 0
        protocol    = -1
        cidr_blocks = [
          "0.0.0.0/0",
        ]
      }
    
    
      egress {
        from_port   = 0
        to_port     = 0
        protocol    = -1
        cidr_blocks = [
          "0.0.0.0/0"
        ]
      }
    
      tags {
        Name = "Terraform Workshop"
      }
    }
    
    ```

## Lekcja 4 - Tworzymy VPC i dorzucamy wirtualki 
https://registry.terraform.io/modules/terraform-aws-modules/vpc/aws/1.12.0
```
module "terraform-workshop" {
  source = "terraform-aws-modules/vpc/aws"

  name = "terraform-workshop"
  cidr = "10.0.0.0/20"

  azs             = ["eu-west-1a", "eu-west-1b"]
  public_subnets  = ["10.0.0.0/24", "10.0.1.0/24"]

  enable_nat_gateway = false
  enable_vpn_gateway = false

  tags = {
    Terraform = "true"
    Environment = "dev"
  }
}
```